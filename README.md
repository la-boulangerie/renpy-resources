# Helpful Ren'Py Links and Info

Author: yam655
Copyright: 2021 CC-BY

## Install and setup Ren'Py Launcher

You will need to download Ren'Py
https://www.renpy.org/

You want to set your "Projects Directory" to be the root of your Ren'Py projects. I use "RenPy Projects" in my Documents folder. You could also put this in your "source" (or "src") directory.

Ren'Py doesn't include an editor by default. If you don't already have a text editor that can handle "rpy" files, you should follow the Ren'Py recommendation of installing Atom.

I know it looks like "jEdit" is a much smaller alternative, but if you don't already have Java installed, installing Java plus jEdit will be much larger than installing Atom.

If you already have Atom (or Notepad++, or Visual Studio Code) installed and they're already opening ".rpy" files, you can just "Use System Editor."

I use the Atom that Ren'Py recommends. That will be well-tested by the Ren'Py folks, and that's what I'm testing for this document.

### Example games and resources

The Ren'Py script syntax is pretty straight-forward. Between "Tutorial" and "The Question" there should be enough to figure out how it works without reading the documentation.

There is good documentation about everything available at https://www.renpy.org/doc/html/

The documentation is important when it comes to knowing text styling options, as well as transition effects for your character sprites.

## Creating a new game

When you're creating a game, you define the resolution for the game.

The greater the resolution, the sharper it looks on large screens, but it also means you need to create larger art. The bigger your art, the more effort it will take.

The height you select when creating the game is the height of your sprite assets. This normally goes from the top of the screen to under the character dialog.

## Character sprites

If there are sprite assets that include feet and it looks like the character is floating in the air, you can adjust the "yalign" to lower them so they are natural.

If your sprite assets are shorter than the height of the screen, they are placed on screen starting at the bottom, not from the top. This means a short sprite will just be short, and not hanging from the top of the screen.

When using profile-style images, please be warned that the characters and background will regularly be seen without the text window under them. It might be nice to think of the sprite as existing just above the text window, but this won't work without compensating for it in other graphic assets. A big profile image is a half body and not a square RPG style portrait.

I ran in to a discussion about background horizon level and placing the character's eyes on that horizon level. -- https://lemmasoft.renai.us/forums/viewtopic.php?t=23117

Ideas to be tested:

1. Consider good looking half-body profiles instead of full-body images. What you decide will impact the width of the characters, and this will impact how many characters can be on-screen at a time. But will you have many scenes when more than three characters are going to be present and it *won't* to feel a little tight?

2. The bigger the character graphics, the less the player will be able to see the background. If the player won't really be seeing it, then it doesn't need to be the fanciest image possible, it just needs to do the job conveying the scene. Don't do more work than you have to do.


## Audio

Instrumental music is really my weakness. But I know some handy tools.

I knew I wanted a key of A minor, but then I just looked around at https://autochords.com/ until I found the "Energetic" feel. That gave me both the primary chords as well as the two alternate sets that I used.

When you know the key and the chords, you can pick things pretty randomly and still get something that sounds good. It doesn't all need to be random, as when things repeat it sounds planned.

I use Hookpad to help with this. https://hookpad.hooktheory.com/ I find it helpful, and I quite like that it can give you LilyPond source files for your songs.

(incomplete)

## Additional resources

Ren'Py "official" dating sim engine
https://github.com/renpy/dse

Ren'Py "official" card game engine
https://github.com/renpy/cardgame

A number of nice looking tutorials
https://zeillearnings.itch.io/

Several examples here:
https://tofurocks.itch.io/
https://lunalucid.itch.io/

Pronouns, Captions, Content Warnings.
https://npckc.itch.io/

Accessibility
https://minute.itch.io/renpy-accessibility

Here's a free Ren'Py guidebook:
https://zishy.itch.io/zys-renpy-guidebook

Interested in adapting a Ren'Py game in to a Godot game?
https://duwangmuffin.itch.io/xdl

Sprite creator and clothing changer:
https://spiralatlas.itch.io/renpy-sprite-creator

RPG logic for Ren'Py:
https://badanni.itch.io/tagon

Upload a game, but lost the source?
https://iwanplays.itch.io/rpaex

Itch image information
https://starwest.itch.io/itch-page-image-templates

